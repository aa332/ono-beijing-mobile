import 'node-libs-react-native/globals';

// library dependencies
import React from 'react';
import { createStackNavigator } from 'react-navigation';

// app dependencies
import Home from './screens/Home';
import ChatCore from './screens/ChatCore';
import Escrow from './screens/Escrow';


const RootStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: { 
        title: 'ono'.toUpperCase(),
        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#eee',
        headerTitleStyle: {
            fontWeight: 'bold'
        }
      }
    },
    Escrow: {
      screen: Escrow,
      navigationOptions: { 
        title: 'escrow'.toUpperCase(),
        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#eee',
        headerTitleStyle: {
            fontWeight: 'bold'
        }
      }
    },
    ChatCore: {
      screen: ChatCore,
      navigationOptions: { 
        title: 'chat'.toUpperCase(),
        headerStyle: {
          backgroundColor: '#000'
        },
        headerTintColor: '#eee',
        headerTitleStyle: {
            fontWeight: 'bold'
        }

      }
    }
  },
  {
    initialRouteName: 'Home'
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

