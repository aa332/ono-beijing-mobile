// library dependencies
import React, { Component, Fragment } from 'react';
import { StatusBar, StyleSheet, Text, Button, TextInput, Dimensions, View } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { GiftedChat } from 'react-native-gifted-chat'


// app dependencies
import withChatKit from '../../components/withChatKit';
import withWeb3 from '../../components/withWeb3';
import TransactionController from '../../components/TransactionController'


const status = {
  READY: 'READY',
  IN_ESCROW: 'IN_ESCROW',
  PAYMENT_SENT: 'PAYMENT_SENT',
  PAYMENT_RECEIVED: 'PAYMENT_RECEIVED'
}

class ChatCore extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chatConnection: null,
      connected: false,
      error: null,
      canConnect: false,
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: "admin",
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        }
      ],
      status: status.READY
    }

    this.loadChat = this.loadChat.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.createGiftedMessage = this.createGiftedMessage.bind(this);
    this._ready = this._ready.bind(this);
    this._inEscrow = this._inEscrow.bind(this);
    this._paymentSent = this._paymentSent.bind(this);
    this._paymentReceived = this._paymentReceived.bind(this);
    this.timer = null;
  }

  loadChat() {
    if ( !this.state.connected ) {
      return (
        <Text> Loading chat... </Text>
      )
    }
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.sendMessage(messages)}
        user={{
          _id: this.props.navigation.getParam('user'),
          name: this.props.navigation.getParam('user')
        }}
      /> 
    )
  }

  sendMessage(messages) {
    messages.forEach( message => {
      this.state.chatConnection.sendMessage({
        text: message.text,
        roomId: this.state.chatConnection.rooms[0].id
      });
    })
  }

  static getDerivedStateFromProps(nextProps) {
    // if ( !nextProps.chatManager || !nextProps.web3 ) {
    //   return null;
    // }
    return {
      canConnect: true,
    }
  }

  createGiftedMessage(message) {
    return {
      _id: message.id,
      text: message.text,
      createdAt: message.createdAt,
      user: {
        _id: message.senderId,
        avatar: 'https://placeimg.com/140/140/any',
      }
    }
  }

  componentDidUpdate() {
    if ( this.state.canConnect && !this.state.chatConnection && !this.state.error ) {
      this.props.chatManager.connect()
      .then( user => {
        user.subscribeToRoom({
          roomId: user.rooms[0].id,
          hooks: {
            onNewMessage: message => {
              console.log(message);
              this.setState(pS => ({
                messages: GiftedChat.append(pS.messages, [this.createGiftedMessage(message)]),
              }))
            }
          },
          messageLimit: 0
        });
        this.setState({ chatConnection: user, connected: true });
      })
      .catch( e => {
        this.setState({ error: e.message })
      })
    }
  }

  _ready(mode) {
    console.log('changing from READY TO IN_ESCROW for: ', mode);
    console.log('depositing ', this.props.navigation.getParam('value'), ' to escrow');
    if ( mode === 'buyer' ){ // if buyer
      setTimeout( () => {
        // this.props.contract.deposit({from: this.props.parties[1], value: this.web3.toWei(this.props.navigation.getParam('value'), "ether"), gas: 3000000}, val => ( console.log(val) ))
        this.setState({status: status.IN_ESCROW})
        clearTimeout(this.timer);
      }, 5000)
      return;
    }
    // if ( !this.state.web3Connection ) {
    //   return;
    // }
    // this.props.contract.deposit({from: this.props.parties[1], value: this.props.web3.toWei(this.props.navigation.getParam('value'), "ether"), gas: 3000000}, (val1, val2) => ( console.log(val1, val2) ))
    this.setState({status: status.IN_ESCROW})
  } 

  _inEscrow(mode) {
    console.log('changing from IN_ESCROW TO PAYMENT_SENT for: ', mode);
    if ( mode === 'buyer' ){ // if buyer
      
      // this.props.contract.releaseEscrow({from: this.props.parties[0]}, (val1, val2) => {
        
      //   console.log(val1, val2)
        
      // })
      
      this.setState({status: status.PAYMENT_SENT});
      
      return;
    }
    setTimeout( () => {
      
      // this.props.contract.releaseEscrow({from: this.props.parties[0]}, (val1, val2) => {
      //     console.log(val1, val2)
      // })

      this.setState({status: status.PAYMENT_SENT});
      
      clearTimeout(this.timer);

    },5000);
  }

  _paymentSent(mode) {
    console.log('changing from PAYMENT_SENT TO PAYMENT_RECEIVED for: ', mode);
    if ( mode === 'buyer' ){ // if buyer
      
      setTimeout( () => {

        // this.props.contract.releaseEscrow({from: this.props.parties[1]}, (val1, val2) => {
        
        //   console.log(val1, val2)
        
        // })
        
        this.setState({status: status.PAYMENT_RECEIVED})
        
        clearTimeout(this.timer);

      }, 5000)

      return;
    }
    
    // this.props.contract.releaseEscrow({from: this.props.parties[1]}, (val1, val2) => {
        
    //     console.log(val1, val2)

    // })

    this.setState({status: status.PAYMENT_RECEIVED})
  }

  _paymentReceived(mode){
    if ( mode === 'buyer' ){ // if buyer
      

    } else { // if seller

    }
    
    // move to next state
    this.navigation.navigate('Completed', { person: mode });
  }


  render () {
    return (
      <SafeAreaView style={{ backgroundColor: '#fff', flex: 1 }}>
        <StatusBar
          barStyle="light-content"
        />
        <TransactionController
          mode={this.props.navigation.getParam('user')}
          status={this.state.status}
          callbacks={{
            READY: this._ready,
            IN_ESCROW: this._inEscrow,
            PAYMENT_SENT: this._paymentSent,
            PAYMENT_RECEIVED: this._paymentReceived
          }}
        />
        { this.loadChat() }
      </SafeAreaView>
    );
  }
}

export default withChatKit(withWeb3(ChatCore));