// library dependencies
import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, Button, ImageBackground, Image, Dimensions, View } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

// app dependencies
import withWeb3 from '../../components/withWeb3'
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      connectionAvailable: false,
      balance: 100.0001,
      sellAmount: 0
    }

    this.navigate = this.navigate.bind(this);
    this.toEscrow = this.toEscrow.bind(this);
  }

  toEscrow() {
    this.props.navigation.navigate('Escrow');
  }

  navigate(type) {
    this.props.navigation.navigate('ChatCore', { user: type, value: this.state.sellAmount });
  }

  static getDerivedStateFromProps(nextProps) {
    if ( !nextProps.parties ) {
      return null;
    }
    return {
      connectionAvailable: true,
    }
  }

  componentDidUpdate() {
    const { web3, parties } = this.props;
    if ( this.state.connectionAvailable && !this.state.balance ) {
  
      // this.props.web3.eth.getBalance(this.props.parties[1], (something, balance) => {
      //   this.setState({balance: this.props.web3.fromWei(balance,'ether')});
      // });
    }
  }

  render () {
    const { height, width } = Dimensions.get('window');
    return (
      <ImageBackground
        source={require('../../assets/background.png')}
        style={{ 
          flex: 1
        }}
      >
        <SafeAreaView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
          <StatusBar
            barStyle="light-content"
          />
          <KeyboardAwareScrollView extraScrollHeight={ 50 } keyboardOpeningTime={ 200 } contentContainerStyle={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
            <Image
              source={require('../../assets/logo_white_3x.png')}
              style={{ height: height/4, width: height/4}}
            />
            <Text style={{ color: '#eee', fontSize: 18 }}>Current balance: {this.state.balance + ' ETH'} </Text>
            <View style={{ width: width-90, margin: 10 }}>
              <FormLabel labelStyle={{ textAlign: 'center', color: '#eee', fontWeight: 'normal' }} >ETH amount</FormLabel>
              <FormInput 
                keyboardType="numeric" 
                onChangeText={val => (this.setState({sellAmount:val}))}
                inputStyle={{ color: '#eee' }}
              /> 
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <Button
                title="BUY"
                color="#eee"
                onPress={ e => ( this.navigate('buyer') ) }
              />
              <Button
                title="SELL"
                color="#eee"
                onPress={ e => (this.navigate('seller') ) }
              />
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

export default withWeb3(Home);