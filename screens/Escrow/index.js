// library dependencies
import 'node-libs-react-native/globals';

import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, Button } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import { SafeAreaView } from 'react-navigation';

//import './global';
import Web3 from 'web3';
import { storageABI } from "../../assets/storage_abi.js";
//console.log(Web3);


class Escrow extends Component {

  constructor(props) {
    super(props);
    this.navigate = this.navigate.bind(this);
    this.deposit = this.deposit.bind(this);
    this.sendDeposit = this.sendDeposit.bind(this);
    this.getParties = this.getParties.bind(this);
    this.state = {amount:0};

    this.web3 = null;
    this.escrow_contract = null;
    this.EscrowContract = null;
    this.parties = null;
  }

  componentDidMount(){
    this.web3 = new Web3(new Web3.providers.HttpProvider("http://10.1.28.250:8545"));
    // console.log(storageABI);
    // var contractAbi = JSON.parse(storageABI);
    this.escrow_contract = this.web3.eth.contract(storageABI);
    console.log(this.escrow_contract);
    try { 
      this.EscrowContract = this.escrow_contract.at('0xb3ecb45ed337393033b70e2b2dd7dc6906717f3f') //initialize smart contract's address
    } catch (e) {
      console.log(e);
    } 
    console.log(this.EscrowContract);
  }

  deposit() {
    result = this.getParties();
    console.log(result);
  }

  getParties(){
     
  }

  sendDeposit() {
    this.EscrowContract.deposit({from: this.parties[1], value: this.web3.toWei(this.state.amount, "ether"), gas: 3000000}, val => ( console.log(val) )); // send ETH from buyer's account to smart contract
    console.log('after use', this.parties);
  }

  navigate() {
    this.props.navigation.navigate('ChatCore');
  }
  render () {
    return (
      <SafeAreaView style={{ backgroundColor: '#eee', flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
        <StatusBar
          barStyle="light-content"
        />
        <Text>{ this.state.amount } ETH</Text>
        <FormLabel>ETH amount</FormLabel>
        <FormInput keyboardType="numeric" onChangeText={val => (this.setState({amount:val}))}/>
        />
        <Button
          title="Deposit in escrow"
          onPress={this.deposit}
        />        
      </SafeAreaView>
    );
  }
}

export default Escrow;