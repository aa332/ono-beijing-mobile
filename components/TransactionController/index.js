import React, { Component, Fragment } from 'react';
import { SafeAreaView } from 'react-navigation';
import { View, Dimensions, Text, StyleSheet } from 'react-native';
import Button from 'react-native-button';

const status = {
  READY: 'READY',
  IN_ESCROW: 'IN_ESCROW',
  PAYMENT_SENT: 'PAYMENT_SENT',
  PAYMENT_RECEIVED: 'PAYMENT_RECEIVED'
}

class TransactionController extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: null,
      status: null
    }
    this.timer = null; // for demo

    this.loadSellerContent = this.loadSellerContent.bind(this);
    this.loadBuyerContent = this.loadBuyerContent.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      mode: nextProps.mode,
      status: status[nextProps.status]
    }
  }

  changeStatus() {
    console.log('changing status', this.state);
    this.props.callbacks[this.state.status](this.state.mode);
    // clearTimeout(this.timer);
  }


  loadSellerContent() {
    switch( this.state.status ) {
      case status.READY: {
        return ( <Fragment><Text>Ready to trade</Text><Button containerStyle={styles.buttonContainer} style={styles.buttonText} onPress={this.changeStatus}>Deposit to escrow</Button></Fragment>)
      }
      case status.IN_ESCROW: {
        this.changeStatus();
        return ( <Fragment><Text>Waiting for buyer to send payment</Text></Fragment>)
      }
      case status.PAYMENT_SENT: {
        return ( <Fragment><Text>Have you received payment?</Text><Button containerStyle={styles.buttonContainer} style={styles.buttonText}  onPress={this.changeStatus}>Confirm receipt</Button></Fragment>)
      }
      case status.PAYMENT_RECEIVED: {
        return ( <Fragment><Text>Transaction complete</Text><Button containerStyle={styles.buttonContainer} style={styles.buttonText}>Exit chat</Button></Fragment>)
      }
    }
  }

  loadBuyerContent() {
    switch( this.state.status ) {
      case status.READY: {
        this.changeStatus();
        return ( <Fragment><Text>Waiting for escrow deposit</Text></Fragment>)
      }
      case status.IN_ESCROW: {
        return ( <Fragment><Text>Have you made a payment?</Text><Button containerStyle={styles.buttonContainer} style={styles.buttonText} onPress={this.changeStatus}>Release escrow</Button></Fragment>)
      }
      case status.PAYMENT_SENT: {
        this.changeStatus();
        return ( <Fragment><Text>Waiting for seller to confirm</Text></Fragment>)
      }
      case status.PAYMENT_RECEIVED: {
        return ( <Fragment><Text>Transaction complete</Text><Button containerStyle={styles.buttonContainer} style={styles.buttonText} onPress={this.changeStatus}>Exit chat</Button></Fragment>)
      }
      default: {
        return (<Text>Something went wrong</Text>)
      }
    }
  }

  render() {
    const { height, width } = Dimensions.get('window');
    return (
      <SafeAreaView style= {{ width: width, height: 50, backgroundColor: '#eee', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>
        { this.state.mode === 'buyer' ? this.loadBuyerContent() : this.loadSellerContent() }
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5, 
    height: 30, 
    overflow: 'hidden', 
    borderRadius: 4, 
    backgroundColor: '#C44778'
  },
  buttonText: {
    color: '#eee',
    fontSize: 14
  }
})
export default TransactionController;