// library dependencies
import React, { Component } from 'react'
import Web3 from 'web3';

// contract details
import { storageABI } from "../../assets/storage_abi.js";

const withWeb3 = (WComponent) => {
  return class Web3Controller extends Component {
    constructor(props) {
      super(props);

      this.web3 = new Web3(new Web3.providers.HttpProvider("http://10.1.28.250:8545"));
      this.escrow_contract = this.web3.eth.contract(storageABI);
      
      this.state = {
        escrow_contract: this.escrow_contract.at('0xfcca2cd6ba5d331bf09e5d3dae7ef06df8018b70'),
        parties: null
      }
    }

    componentDidMount() {
      this.state.escrow_contract.getParties( (something, values) =>{
        this.setState({
          parties: values
        });
      })
    }

    render (){
      return (
        <WComponent parties={this.state.parties} contract={this.state.escrow_contract} web3={this.web3} {...this.props} />
      )
    }
  }
}

export default withWeb3;