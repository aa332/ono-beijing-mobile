import React, { Component } from 'react';
import io from 'socket.io-client';

const withSocketServer = (WComponent) => {
  return class SocketServer extends Component {
    constructor(props) {
      super(props);
      this.SocketClient = io('http://localhost:3000');
      console.log(this.SocketClient);
    }
    render() {
      return (
          <WComponent socketClient={this.SocketClient} {...this.props} />
      )
    }
  }
}

export default withSocketServer