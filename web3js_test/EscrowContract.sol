pragma solidity ^0.4.11;

contract EscrowContract{
    uint balance;
    address public buyer;
    address public seller;
    address private escrow;
    uint private start;
    bool buyerOk;
    bool sellerOk;
    
    function Escrow(address buyer_address, address seller_address) public {
        // this is the constructor function that runs ONCE upon initialization
        buyer = buyer_address;
        seller = seller_address;
        escrow = msg.sender;
        balance = address(this).balance;
        start = now; //now is an alias for block.timestamp, not really "now"
    }
    
    function releaseEscrow() public {
    //require(msg.sender == buyer || msg.sender == seller);
        if (msg.sender == buyer){
            buyerOk = true;
        } else if (msg.sender == seller){
            sellerOk = true;
        }
        if (buyerOk && sellerOk) {
            payBalance();
        } else if (buyerOk && !sellerOk && now > start + 30 days){
            //Freeze 30 days before release to buyer. The customer has to remember to call this method after freeze period.
            selfdestruct(buyer);
        }
    }
    
    function payBalance() private {
        // send buyer the balance
        if (buyer.send(address(this).balance)){
            balance = 0;
        } else {
            revert();
        }
    }
    function deposit() public payable {
        require(msg.sender == seller);
        require (balance == 0);
        balance += msg.value;
    }
    
    function cancel() public {
        if (msg.sender == buyer){
            buyerOk = false;
        } else if (msg.sender == seller){
            sellerOk = false;
        }
        // if both buyer and seller would like to cancel, money is returned to buyer 
        if (!buyerOk && !sellerOk){
            selfdestruct(buyer);
        }
    }
    
    function getBalance() public view returns(uint){
        return address(this).balance;
    }
    
    function getParties() public view returns(address, address){
        return (buyer, seller);
    }

    function kill() private constant {
        if (msg.sender == escrow) {
            selfdestruct(buyer);
        }
        
    }
}

