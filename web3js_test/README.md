
This folder contains simple implementation of web3js interaction with Ethereum's test net

To test the following code you need Node.js and NPM

run

''' 
npm install -g ethereumjs-testrpc
''' 

then start it

''' 
testrpc
''' 

In separate terminal window run

'''
npm init

npm install ethereum/web3.js --save
'''

copy the code from EscrowContract.sol to http://remix.ethereum.org/ compile code and deploy it to "Web3 Provider" of your test network

Change the contract's address in EscrowContract variable in both innit.html and transaction.html to contract's address on your local network

In innit.html you can initialize Buyer's and Seller's wallets

In transaction.html you can generate transactions on Ethereum network

You can run getParties on Remix to test if wallets were intialised on the network, as well as getBalance in order to get the smart contract's balance
